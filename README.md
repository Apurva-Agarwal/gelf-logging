# gelf-logging project

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```shell script
./mvnw compile quarkus:dev
```

## To build and start fluentd,elasticsearch and kibana containers
```Use command = <docker-compose up>  inside fluentd folder
``Check if all containers are started using <docker ps> command
``Once containers are up navigate to <http://localhost:5601/> to see logs arriving in kibana dashboard in the respective index pattern created
``Check logs of fluentd container using command <docker logs container_name --follow>
